import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CreateMovieDto } from './dto/create-movie.dto';
import { UpdateMovieDto } from './dto/update-movie.dto';
import { Movie } from './entities/movie.entity';

@Injectable()
export class MoviesService {
  constructor(
    @Inject('MOVIE_MODEL')
    private movieModel: Model<Movie>,
  ) {}
  create(createMovieDto: CreateMovieDto) {
    const createdMovie = new this.movieModel(createMovieDto);
    return createdMovie.save();
  }

  findAll(offset: number, limit: number) {
    return this.movieModel
      .find()
      .skip(offset > 0 ? (offset - 1) * limit : 0)
      .limit(limit)
      .sort({ release_date: 1 })
      .exec();
  }

  findOne(id: string) {
    return this.movieModel.findOne({ _id: id });
  }

  update(id: string, updateMovieDto: UpdateMovieDto) {
    return this.movieModel.findOneAndUpdate({ _id: id }, { updateMovieDto });
  }

  remove(id: string) {
    return this.movieModel.remove({ _id: id });
  }
}
