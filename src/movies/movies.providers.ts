import { Connection } from 'mongoose';
import { MovieSchema } from './entities/movie.entity';

export const moviesProviders = [
  {
    provide: 'MOVIE_MODEL',
    useFactory: (connection: Connection) =>
      connection.model('Movie', MovieSchema),
    inject: ['DATABASE_CONNECTION'],
  },
];
