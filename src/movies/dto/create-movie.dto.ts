export class CreateMovieDto {
  title: string;
  original_title: string;
  movie_banner: string;
  description: string;
  release_date: string;
  rt_score: string;
}
