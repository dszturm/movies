import { Test, TestingModule } from '@nestjs/testing';
import { DatabaseModule } from '../database/database.module';
import { MoviesController } from './movies.controller';
import { moviesProviders } from './movies.providers';
import { MoviesService } from './movies.service';

describe('MoviesController', () => {
  let controller: MoviesController;
  let service: MoviesService;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [DatabaseModule],
      controllers: [MoviesController],
      providers: [MoviesService, ...moviesProviders],
    }).compile();

    controller = module.get<MoviesController>(MoviesController);
    service = module.get<MoviesService>(MoviesService);
  });

  describe('findAll', () => {
    it('should return an array of cats', async () => {
      const result = [
        {
          _id: '637b2c651d9edcdfa9cf5ea4',
          title: 'Castle in the Sky',
          original_title: '天空の城ラピュタ',
          description:
            "The orphan Sheeta inherited a mysterious crystal that links her to the mythical sky-kingdom of Laputa. With the help of resourceful Pazu and a rollicking band of sky pirates, she makes her way to the ruins of the once-great civilization. Sheeta and Pazu must outwit the evil Muska, who plans to use Laputa's science to make himself ruler of the world.",
          release_date: '1986',
          rt_score: '95',
        },
        {
          _id: '637b2c651d9edcdfa9cf5ea5',
          title: 'Grave of the Fireflies',
          original_title: '火垂るの墓',
          description:
            'In the latter part of World War II, a boy and his sister, orphaned when their mother is killed in the firebombing of Tokyo, are left to survive on their own in what remains of civilian life in Japan. The plot follows this boy and his sister as they do their best to survive in the Japanese countryside, battling hunger, prejudice, and pride in their own quiet, personal battle.',
          release_date: '1988',
          rt_score: '97',
        },
        {
          _id: '637b2c651d9edcdfa9cf5ea6',
          title: 'My Neighbor Totoro',
          original_title: 'となりのトトロ',
          description:
            'Two sisters move to the country with their father in order to be closer to their hospitalized mother, and discover the surrounding trees are inhabited by Totoros, magical spirits of the forest. When the youngest runs away from home, the older sister seeks help from the spirits to find her.',
          release_date: '1988',
          rt_score: '93',
        },
      ];
      jest.spyOn(controller, 'findAll');

      expect(await controller.findAll(1, 3)).toHaveLength(1);
    });
  });
});
